/* eslint-disable */
import P5 from 'p5/lib/p5.min'

const script = function(p5) {
  const dotCount = 400;
  const maxSize = 6;
  const minSize = 3;
  let bg = 0;
  let targetBg = 10;

  class Thingy {
    constructor() {
      this.spIdentity = p5.random(minSize, maxSize)
      this.pos = p5.createVector(p5.random(p5.width), p5.random(p5.height))
      this.dir = p5.createVector(p5.random(this.spIdentity * -1, this.spIdentity), p5.random(this.spIdentity * -1, this.spIdentity)).normalize()
      this.size = maxSize - this.spIdentity
      this.target = false
      this.maxTargetDistance = 30
    }

    move() {
      if (this.target) {
        if(p5.dist(this.target.x, this.target.y, this.pos.x, this.pos.y) < this.maxTargetDistance) {
          this.setTarget(this.findNewPossiblePoint())
        }

        const cl = p5.createVector(this.target.x, this.target.y)
        this.dir = cl.sub(this.pos).div(15)
      }

      this.pos.add(this.dir)
      if (this.pos.x > p5.width || this.pos.x < 0) {
        this.dir.x *= -1
      }

      if (this.pos.y > p5.height || this.pos.y < 0) {
        this.dir.y *= -1
      }
    }

    draw() {
      this.move()

      p5.colorMode(p5.HSB)
      p5.stroke(p5.map(this.dir.mag(), 0, 10, 50, 100), 60, 100)
      p5.strokeWeight(this.size)
      p5.point(this.pos.x, this.pos.y)
      p5.strokeWeight(this.size)

    }

    findNewPossiblePoint() {
      let newPoint = this.getScaledOffsetVector(p5.random(this.possiblePoints));
      let tries = 0;
      while((p5.dist(this.target.x, this.target.y, newPoint.x, newPoint.y) > this.maxTargetDistance) && tries < 500) {
        newPoint = this.getScaledOffsetVector(p5.random(this.possiblePoints))
        tries++
      }
      return newPoint
    }

    getScaledOffsetVector(vector) {
      let calculatedOffset = p5.createVector(this.targetOffset.x, this.targetOffset.y);

      if (p5.width < 768) {
        calculatedOffset = p5.createVector(p5.width / 2 - this.targetImageWidth * this.targetScale / 2, p5.height / 2 - this.targetImageHeight / 2);
      }

      return p5.createVector(vector.x * this.targetScale + calculatedOffset.x,
        vector.y * this.targetScale + calculatedOffset.y)
    }

    setTarget(vector) {
      this.target = vector
    }

    goTowards(points, offset, scale = 1, imgWidth, imgHeight) {
      this.targetImageWidth = imgWidth
      this.targetImageHeight = imgHeight
      this.possiblePoints = points
      this.targetScale = scale
      this.targetOffset = p5.createVector(offset.x, offset.y)
      let randomPoint = p5.random(this.possiblePoints)
      this.setTarget(this.getScaledOffsetVector(randomPoint))
    }

    freeRoam() {
      this.target = false
    }
  }

  function getImagePoints(img) {
    img.loadPixels()
    const points = [];

    for(let x = 0; x < img.width; x++) {
      for(let y = 0; y < img.height; y++) {
        const index = (x + y * img.width)*4;
        const b = (img.pixels[index] + img.pixels[index + 1] + img.pixels[index + 2]) / 3
        if (b > 50) {
          points.push(p5.createVector(x, y))
        }
      }
    }

    return points
  }

  const thingies = []

  let imgA
  let imgC
  let imgW

  p5.preload = _ => {
    imgA = p5.loadImage('/image/a.png')
    imgC = p5.loadImage('/image/c.png')
    imgW = p5.loadImage('/image/w.png')
  }

  p5.windowResized = _ => {
    p5.resizeCanvas(window.innerWidth, window.innerHeight)
  }

  p5.setup = _ => {
    p5.createCanvas(window.innerWidth, window.innerHeight);

    for (let i = 0; i < dotCount; i++) {
      thingies.push(new Thingy())
    }

    const pointsAbout = getImagePoints(imgA)
    const pointsWork = getImagePoints(imgW)
    const pointsContact = getImagePoints(imgC)

    window.addEventListener('show-hero', () => {
      thingies.forEach((t) => {
        t.freeRoam()
      })
      targetBg = 10
    })

    window.addEventListener('show-about', () => {
      thingies.forEach((t) => {
        t.goTowards(pointsAbout, p5.createVector(p5.width * 0.75 - imgA.width / 2, p5.height / 2 - imgA.height / 2), 1, imgA.width, imgA.height)
      })
      targetBg = 13
    })

    window.addEventListener('show-works', () => {
      thingies.forEach((t) => {
        t.goTowards(pointsWork, p5.createVector(p5.width * 0.25 - imgW.width / 2, p5.height / 2 - imgW.height / 2), 1, imgW.width, imgW.height)
      })
      targetBg = 16
    })

    window.addEventListener('show-contact', () => {
      thingies.forEach((t) => {
        t.goTowards(pointsContact, p5.createVector(p5.width * 0.75 - imgC.width, p5.height / 2 - imgC.height), 2, imgC.width, imgW.height)
      })
      targetBg = 20
    })
  }

  p5.draw = _ => {
    p5.clear()
    bg = p5.lerp(bg, targetBg, 0.1)
    p5.background(0,0, bg)
    p5.translate(p5.width / 2, p5.height / 2)
    p5.translate(-p5.width / 2, -p5.height / 2)
    thingies.forEach(t => t.draw())
  }
}

class P5Script {
  constructor(script) {
    this.script = script
  }

  init(node, sync) {
    new P5(this.script, node, sync)
  }
}

export default (ctx, inject) => {
  const p5s = new P5Script(script)
  ctx.$p5 = p5s
  inject('p5', p5s)
}
