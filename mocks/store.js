import Vue from 'vue'
import Vuex from 'vuex'
import { actions, getters, mutations, state } from '../store/index'

Vue.use(Vuex)

export default new Vuex.Store({ actions, getters, mutations, state })
