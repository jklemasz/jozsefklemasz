export default class AxiosMock {
  constructor(successful) {
    this.successful = successful
  }

  post() {
    return new Promise((resolve, reject) => {
      if (this.successful) {
        resolve('success')
      } else {
        reject(new Error('failed'))
      }
    })
  }
}
