export const state = () => ({
  loaded: false
})

export const mutations = {
  SET_LOADED(state, loaded) {
    state.loaded = loaded
  }
}

export const getters = {
  loaded: state => state.loaded
}

export const actions = {
  setLoaded({ commit }, loaded) {
    commit('SET_LOADED', loaded)
  }
}
