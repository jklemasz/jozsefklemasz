export default class Work {
  constructor(name, url, tags) {
    this.name = name
    this.url = url
    this.tags = Array.from(tags)
  }
}
