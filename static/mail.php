<?php

$origin = $_SERVER['HTTP_ORIGIN'];
$allowed_domains = [
    'https://jozsefklemasz.com',
    'https://jozsefklemasz.hu',
    'https://jozsefklemasz.de',
];

if (in_array($origin, $allowed_domains)) {
    header('Access-Control-Allow-Origin: ' . $origin);
}

$formData = filter_input_array(INPUT_POST, $_POST);

$langs = [
    'en' => [
        'subject' => 'Contact from: '
    ],
    'hu' => [],
    'de' => []
];

if($formData['name'] && $formData['email'] && $formData['message'] && $formData['locale']) {
  $to = 'jozsefklemasz@gmail.com';
  $subject = $langs[$formData['locale']]['subject'] . $formData['email'];
  $message = strip_tags($formData['message']);
  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $headers .= "From: <" . $formData['email'] . ">\r\n";

  mail($to, $subject, $message, $headers);
}