const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#f00' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/p5', ssr: false },
    { src: '~/plugins/velocity', ssr: false },
    { src: '~/plugins/localStorage.js', ssr: false },
    { src: '~/plugins/eventbus' }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    [ '@nuxtjs/axios', {
      baseURL: 'https://jozsefklemasz.hu',
      https: true
    } ],
    [ 'nuxt-fontawesome', {
      component: 'fa',
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['faHome', 'faUserCircle', 'faBriefcase', 'faAt', 'faArrowDown', 'faBars']
        }
      ]
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-139136022-2'
    }],
    [ 'nuxt-i18n' ]
  ],

  i18n: {
    locales: [
      {
        code: 'en',
        iso: 'en-US'
      },
      {
        code: 'hu',
        iso: 'hu-HU'
      },
      {
        code: 'de',
        iso: 'de-DE'
      }],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        'en': require('./locales/en.json'),
        'hu': require('./locales/hu.json'),
        'de': require('./locales/de.json')
      }
    }
  },

  robots: {
    UserAgent: '*',
    Disallow: '/admin'
  },

  sitemap: {
    hostname: 'https://jozsefklemasz.hu',
    gzip: true,
    routes: [
      '/'
    ]
  },

  /*
  ** Build configuration
  */
  build: {
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
