import { shallowMount } from '@vue/test-utils'
import About from '~/components/sections/About.vue'

const factory = () => {
  return shallowMount(About, {
    mocks: {
      $t: msg => msg
    }
  })
}

describe('About', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })
})
