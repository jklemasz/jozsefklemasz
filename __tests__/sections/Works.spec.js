import { shallowMount } from '@vue/test-utils'
import Works from '~/components/sections/Works.vue'

const factory = () => {
  return shallowMount(Works, {
    mocks: {
      $t: msg => msg
    }
  })
}

describe('Works', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBe(true)
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('resetkeyword and activateKeywords', () => {
    const wrapper = factory()
    const work = wrapper.vm.works[0]

    expect(wrapper.html()).toMatchSnapshot()

    wrapper.vm.activateKeywords(work)
    wrapper.vm.keywords.forEach((keyword) => {
      if (work.tags.includes(keyword.name)) {
        expect(keyword.active).toBe(true)
      } else {
        expect(keyword.active).toBe(false)
      }
    })

    wrapper.vm.resetKeywords()

    wrapper.vm.keywords.forEach((keyword) => {
      expect(keyword.active).toBe(false)
    })
  })
})
