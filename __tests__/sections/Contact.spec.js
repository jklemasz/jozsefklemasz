import Vue from 'vue'
import { shallowMount } from '@vue/test-utils'
import Contact from '~/components/sections/Contact.vue'
import AxiosMock from '~/mocks/axios'
import store from '~/mocks/store'

const factory = () => {
  return shallowMount(Contact, {
    mocks: {
      $t: msg => msg
    },
    store
  })
}

describe('Contact', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBe(true)
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('setters', () => {
    const wrapper = factory()

    wrapper.vm.setNameError('nameerror')
    expect(wrapper.vm.nameError).toBe('nameerror')

    wrapper.vm.setName('name')
    expect(wrapper.vm.name).toBe('name')

    wrapper.vm.setEmailError('emailerror')
    expect(wrapper.vm.emailError).toBe('emailerror')

    wrapper.vm.setEmail('email')
    expect(wrapper.vm.email).toBe('email')

    wrapper.vm.setMessageError('messageError')
    expect(wrapper.vm.messageError).toBe('messageError')

    wrapper.vm.setMessage('message')
    expect(wrapper.vm.message).toBe('message')
  })

  test('send message sucessful', async () => {
    const wrapper = shallowMount(Contact, {
      mocks: {
        $t: msg => msg,
        $axios: new AxiosMock(true)
      },
      store
    })

    wrapper.vm.setName('John Johnson')
    wrapper.vm.setEmail('jojohnson@test.test')
    wrapper.vm.setMessage('Test email message')

    wrapper.vm.sendMessage()
    await Vue.nextTick()
    expect(wrapper.vm.success).toBe('contact.submit.success')
  })

  test('send message fails', async () => {
    const wrapper = shallowMount(Contact, {
      mocks: {
        $t: msg => msg,
        $axios: new AxiosMock(false)
      },
      store
    })

    wrapper.vm.setName('John Johnson')
    wrapper.vm.setEmail('jojohnson@test.test')
    wrapper.vm.setMessage('Test email message')

    wrapper.vm.sendMessage()
    await Vue.nextTick()
    expect(wrapper.vm.error).toBe('contact.submit.error')
  })
})
