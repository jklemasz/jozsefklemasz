import { shallowMount } from '@vue/test-utils'
import eventbus from '~/mocks/eventbus'
import store from '~/mocks/store'
import Hero from '~/components/sections/Hero.vue'

const factory = () => {
  return shallowMount(Hero, {
    mocks: {
      $t: msg => msg,
      $bus: eventbus
    },
    stubs: ['fa'],
    store
  })
}

describe('Hero', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })
})
