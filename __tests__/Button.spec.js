import { shallowMount } from '@vue/test-utils'
import Button from '~/components/Button.vue'

const factory = () => {
  return shallowMount(Button, {
    propsData: {
      href: '#test-href'
    },
    mocks: {
      $t: msg => msg
    }
  })
}

describe('Button', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('href binds properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toContain('href="#test-href')
  })
})
