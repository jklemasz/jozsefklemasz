import { shallowMount } from '@vue/test-utils'
import InputField from '~/components/InputField.vue'

const factory = () => {
  return shallowMount(InputField, {
    mocks: {
      $t: msg => msg
    }
  })
}

describe('InputField', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders properly with default props', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('textarea renders properly', () => {
    const wrapper = shallowMount(InputField, {
      mocks: {
        $t: msg => msg
      },
      propsData: {
        inputType: 'textarea'
      }
    })
    expect(wrapper.html()).toContain('<textarea class="">')
  })
})
