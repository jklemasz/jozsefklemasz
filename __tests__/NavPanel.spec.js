import { shallowMount } from '@vue/test-utils'
import NavPanel from '~/components/NavPanel.vue'
import store from '~/mocks/store'
import EventBus from '~/mocks/eventbus'

const factory = () => {
  return shallowMount(NavPanel, {
    mocks: {
      $t: msg => msg,
      $bus: EventBus
    },
    stubs: ['fa'],
    store,
    propsData: {
      current: 0
    }
  })
}

describe('NavPanel', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBe(true)
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('isActive method', () => {
    const wrapper = factory()
    expect(wrapper.vm.isActive(0)).toBe(true)
    expect(wrapper.vm.isActive(1)).toBe(false)
  })

  test('navigate method', async () => {
    const wrapper = factory()
    wrapper.vm.navigateTo(0)
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().navigate).toBeTruthy()
    expect(wrapper.emitted().navigate.length).toBe(1)
    expect(wrapper.emitted().navigate[0]).toEqual([0])
  })

  test('toggleNav method', () => {
    const wrapper = factory()
    wrapper.vm.closed = false
    wrapper.vm.toggleNav()
    expect(wrapper.vm.closed).toBe(true)
    wrapper.vm.toggleNav()
    expect(wrapper.vm.closed).toBe(false)
  })
})
