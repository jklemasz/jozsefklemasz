import { shallowMount } from '@vue/test-utils'
import LanguageSelector from '~/components/LanguageSelector.vue'

const factory = () => {
  return shallowMount(LanguageSelector, {
    mocks: {
      $t: msg => msg,
      switchLocalePath: msg => msg
    },
    stubs: ['nuxt-link'],
    propsData: {
      lang: 'hu'
    }
  })
}

describe('LanguageSelector', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders properly with default props', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('can switch language', () => {
    const wrapper = factory()
    const selector = wrapper.find('.c-language-selector')
    selector.trigger('click')
  })
})
