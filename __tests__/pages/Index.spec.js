import { shallowMount } from '@vue/test-utils'
import Index from '~/pages/index'
import P5Mock from '~/mocks/p5'
import EventBus from '~/mocks/eventbus'
import velocity from '~/mocks/velocity'

const factory = () => {
  return shallowMount(Index, {
    propsData: {
      href: '#test-href'
    },
    mocks: {
      $t: msg => msg,
      $p5: new P5Mock(),
      $bus: EventBus,
      $velocity: velocity,
      $route: {
        hash: ''
      }
    },
    stubs: ['no-ssr']
  })
}

describe('Index', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    console.log(wrapper.vm.$p5)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })
})
