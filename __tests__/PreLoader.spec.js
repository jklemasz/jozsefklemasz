import { shallowMount } from '@vue/test-utils'
import PreLoader from '~/components/PreLoader.vue'
import store from '~/mocks/store'
import velocity from '~/mocks/velocity'
import EventBus from '~/mocks/eventbus'

const factory = () => {
  return shallowMount(PreLoader, {
    mocks: {
      $bus: EventBus,
      $velocity: velocity
    },
    store
  })
}

describe('PreLoader', () => {
  test('mounts properly', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBe(true)
  })

  test('renders properly', () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })
})
